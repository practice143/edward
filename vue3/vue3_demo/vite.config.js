import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'

// https://vitejs.dev/config/
export default defineConfig({
  // 修改启动端口serve:{ port : 8080}
  server:{
    host: 'localhost',
    port:8080
  },
  plugins: [vue()]
})
