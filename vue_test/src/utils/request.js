import axios from 'axios'

const service = axios.create({
  baseURL: 'url',
  timeout: 5000
})

// 请求拦截
// 所有的网络请求都会先走这个方法
service.interceptors.request.use(req => {
  // 部分请求需要token
  // 先获取token
  const token = localStorage.getItem('token')
  //   判断是否存在token
  if (token) {
    // headers.token token名称不一定，也有可能叫别的名字
    req.headers.token = token
  }
  return req
}, err => {
  return Promise.reject(err)
})

// 响应拦截
// 服务器返回数据之后都会先执行此方法
service.interceptors.response.use(res => {

})

// 整体导出
export default service
