import { ADD } from './mutations-type'
export default {
  [ADD] (state, {index}) {
    state.goods[index].num++
    state.totalNum++
    state.totalPrice += state.goods[index].price
  }
}
